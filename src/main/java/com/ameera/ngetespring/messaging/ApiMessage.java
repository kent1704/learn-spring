/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetespring.messaging;


/**
 *
 * @author ivanferianda
 */
public class ApiMessage {
    boolean status;
    String message;
    Long response_code;

    public ApiMessage() {
    }
    
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getResponse_code() {
        return response_code;
    }

    public void setResponse_code(Long response_code) {
        this.response_code = response_code;
    }
    
}
