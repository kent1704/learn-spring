/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.repository;

import com.ameera.ngetestspring.model.BarangDagang;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author wahyu
 */
public interface BarangDagangRepository extends JpaRepository<BarangDagang,Long>{
    
}
