/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.repository;

import com.ameera.ngetestspring.model.Toko;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author wahyu
 */
public interface TokoRepository extends JpaRepository<Toko,Long>{
    
    @Query(value="select t from Toko t "
                +"where t.name= :namaToko")
    Toko findTokoByNamaToko(@Param("namaToko") String nama);
    
}
