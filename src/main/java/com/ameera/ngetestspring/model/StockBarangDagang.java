/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.model;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kentvanlim
 */
@Table(name="trx_stock_barang")
public class StockBarangDagang {
@Id
@GeneratedValue(strategy= GenerationType.IDENTITY)

@ManyToOne(cascade =CascadeType.ALL)
@JoinColumn(name="Barang_Dagang_Id",referencedColumnName="id",updatable=true,nullable=true)
private BarangDagang barangDagang;

@ManyToOne
@JoinColumn(name="purchase_order_barang",referencedColumnName="id",updatable=true,nullable=true)
private PurchaseOrder purchaseOrder;

private Long price;
private Long stokAwal;
private Long jumlahTerpakai;
private Byte status;

public StockBarangDagang(){
    
}

    public BarangDagang getBarangDagang() {
        return barangDagang;
    }

    public void setBarangDagang(BarangDagang barangDagang) {
        this.barangDagang = barangDagang;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getStokAwal() {
        return stokAwal;
    }

    public void setStokAwal(Long stokAwal) {
        this.stokAwal = stokAwal;
    }

    public Long getJumlahTerpakai() {
        return jumlahTerpakai;
    }

    public void setJumlahTerpakai(Long jumlahTerpakai) {
        this.jumlahTerpakai = jumlahTerpakai;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public StockBarangDagang(BarangDagang barangDagang, PurchaseOrder purchaseOrder, Long price, Long stokAwal, Long jumlahTerpakai, Byte status) {
        this.barangDagang = barangDagang;
        this.purchaseOrder = purchaseOrder;
        this.price = price;
        this.stokAwal = stokAwal;
        this.jumlahTerpakai = jumlahTerpakai;
        this.status = status;
    }

}
