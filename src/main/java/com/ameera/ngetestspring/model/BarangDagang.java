/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author wahyu
 */

@Entity
@Table(name = "barang_dagang")
public class BarangDagang {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "nama", nullable = false)
    private String name;
    @Column(name = "harga", nullable = false)
    private Integer harga;
    
    
    @ManyToOne
    @JoinColumn(name = "toko_id",referencedColumnName = "id",updatable = true,nullable = false)
    private Toko toko;

    public BarangDagang() {
    }

    public BarangDagang(String name, Integer harga, Toko toko) {
        this.name = name;
        this.harga = harga;
        this.toko = toko;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Toko getToko() {
        return toko;
    }

    public void setToko(Toko toko) {
        this.toko = toko;
    }

    
}
