/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.wrapper;

/**
 *
 * @author wahyu
 */
public class WrapperDisplayBarang {
    private Long id;

    public WrapperDisplayBarang(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    private String nama, namaToko;
    private Integer harga;

    public WrapperDisplayBarang() {
    }

    public WrapperDisplayBarang(String nama, String namaToko, Integer harga) {
        this.nama = nama;
        this.namaToko = namaToko;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }
    
    
}
