/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.controller;

import com.ameera.ngetespring.messaging.ApiMessage;
import com.ameera.ngetestspring.model.BarangDagang;
import com.ameera.ngetestspring.model.Toko;
import com.ameera.ngetestspring.repository.BarangDagangRepository;
import com.ameera.ngetestspring.repository.TokoRepository;
import com.ameera.ngetestspring.wrapper.WrapperDisplayBarang;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wahyu
 */
@RestController
@RequestMapping(path = "barang")
public class ControllerBarang {
    private ApiMessage apimessage = new ApiMessage();
    @Autowired
    private BarangDagangRepository barangRepository;
    @Autowired
    private TokoRepository tokoRepository;

    @RequestMapping(value = "view", method = RequestMethod.GET,
            produces = "application/json")
    public @ResponseBody
    List<BarangDagang> viewAll() {
        return barangRepository.findAll();
    }
    
    @RequestMapping(value="/delete",method = RequestMethod.GET,produces = "application/json")
    public @ResponseBody ApiMessage delete(@RequestParam (name = "id", required = true)Long id) {
          try{
              barangRepository.delete(id);
              apimessage.setMessage("Berhasil Cok");
              apimessage.setStatus(true);
          }catch(Exception e){
              apimessage.setMessage("Gagal Cok");
              apimessage.setStatus(false);
          }
          return apimessage;
    }
    @Transactional
    @RequestMapping(value = "add", method = RequestMethod.POST,
            consumes = "application/json", produces = "application/json")
    public @ResponseBody
    BarangDagang addBarang(@RequestBody BarangDagang barang) {
        BarangDagang hasilSave = barangRepository.save(barang);
        return hasilSave;
    }

    @Transactional
    @RequestMapping(value = "addBarangPakeWrapper", method = RequestMethod.POST,
            consumes = "application/json", produces = "application/json")
    public @ResponseBody
    BarangDagang addTokoPakeModel(@RequestBody WrapperDisplayBarang barangDariWrapper) {
        Toko tokoKepilih = tokoRepository.findTokoByNamaToko(barangDariWrapper.getNamaToko());
        BarangDagang barang = new BarangDagang(barangDariWrapper.getNama(),
                barangDariWrapper.getHarga(), tokoKepilih);
        BarangDagang barangSave = barangRepository.save(barang);
        return barangSave;
    }

    @Transactional
    @RequestMapping(value = "updateBarang", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    BarangDagang updateToko(@RequestBody WrapperDisplayBarang barangDariWrapper) {

        BarangDagang barang = barangRepository.findOne(barangDariWrapper.getId());
        if (!"".equals(barangDariWrapper.getHarga()) && barangDariWrapper.getHarga() != null) {
            barang.setHarga(barangDariWrapper.getHarga());
        }
        if (!"".equals(barangDariWrapper.getNama()) && barangDariWrapper.getNama() != null) {
            barang.setName(barangDariWrapper.getNama());
        }
        BarangDagang barangSave = barangRepository.save(barang);
        return barangSave;
    }
}
