/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameera.ngetestspring.controller;

import com.ameera.ngetespring.messaging.ApiMessage;
import com.ameera.ngetestspring.model.Toko;
import com.ameera.ngetestspring.repository.TokoRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wahyu
 */

@RestController
@RequestMapping(path = "toko")
public class ControllerToko {
    private ApiMessage apimessage = new ApiMessage();
    @Autowired
    private TokoRepository tokoRepository;
    
     @RequestMapping(value="viewAllToko",method=RequestMethod.GET,produces = "application/json")
    public @ResponseBody List<Toko> liatToko(){
        return tokoRepository.findAll();
    }
    @Transactional
    @RequestMapping(value = "addTokoPakeModel", method = RequestMethod.POST, 
            consumes = "application/json", produces = "application/json")
    public @ResponseBody Toko addTokoPakeModel(@RequestBody Toko tokoModel) 
    {
       Toko tokoSaved = tokoRepository.save(tokoModel);
       return tokoSaved;
    }
   
    
       @RequestMapping(value="/delete",method = RequestMethod.GET,produces = "application/json")
    public @ResponseBody ApiMessage deleteItem(@RequestParam (name = "id", required = true)Long id) {
          try{
              tokoRepository.delete(id);
              apimessage.setMessage("Berhasil Cok");
              apimessage.setStatus(true);
          }catch(Exception e){
              apimessage.setMessage("Gagal Cok");
              apimessage.setStatus(false);
          }
          return apimessage;
    }
}
